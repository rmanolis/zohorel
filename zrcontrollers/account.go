package zrcontrollers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"zohorel/zrglobals"
	"zohorel/zrmodels"
	"zohorel/zrutils"

	"github.com/gorilla/mux"
)

func GetByAccId(w http.ResponseWriter, r *http.Request) {
	db := zrutils.GetDB(r, zrglobals.GetDBName())
	vars := mux.Vars(r)
	id := vars["id"]
	acc := new(zrmodels.Account)
	err := acc.ByAccountId(db, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	out, err := json.Marshal(acc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}

func GetByNo(w http.ResponseWriter, r *http.Request) {
	db := zrutils.GetDB(r, zrglobals.GetDBName())
	vars := mux.Vars(r)
	id := vars["id"]
	acc := new(zrmodels.Account)
	no, err := strconv.Atoi(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = acc.ByNo(db, no)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	out, err := json.Marshal(acc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}

func GetByAccNum(w http.ResponseWriter, r *http.Request) {
	db := zrutils.GetDB(r, zrglobals.GetDBName())
	vars := mux.Vars(r)
	id := vars["id"]
	acc := new(zrmodels.Account)
	err := acc.ByAccountNum(db, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	out, err := json.Marshal(acc)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write(out)
}
