package zrcontrollers

import (
	"net/http"
	"zohorel/zrutils"

	"github.com/gorilla/mux"
)

func Welcome(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "static/index.html")
}

func LoadRoutes() *mux.Router {
	router := mux.NewRouter()
	router.HandleFunc("/", Welcome).Methods("GET")
	a := router.PathPrefix("/auth").Subrouter()
	a.HandleFunc("/login", Login).Methods("POST")
	a.HandleFunc("/logout", zrutils.AuthHandler(Logout)).Methods("GET")
	a.HandleFunc("/user", IsUser).Methods("GET")

	//router.HandleFunc("/load/accounts",
	//	zrutils.AuthHandler(LoadAccounts)).Methods("GET")
	router.HandleFunc("/load_accounts/{to}/{from}",
		AuthTokenHandler(LoadAccounts)).Methods("GET")

	router.HandleFunc("/account_ids/{id}",
		AuthTokenHandler(GetByAccId)).Methods("GET")
	router.HandleFunc("/no/{id}",
		AuthTokenHandler(GetByNo)).Methods("GET")
	router.HandleFunc("/account_numbers/{id}",
		AuthTokenHandler(GetByAccNum)).Methods("GET")

	router.HandleFunc("/users",
		zrutils.AuthHandler(ListUsers)).Methods("GET")
	router.HandleFunc("/users",
		zrutils.AuthHandler(AddUser)).Methods("POST")
	router.HandleFunc("/users",
		zrutils.AuthHandler(EditUser)).Methods("PUT")

	router.HandleFunc("/config",
		zrutils.AuthHandler(EditConfig)).Methods("PUT")
	router.HandleFunc("/config",
		zrutils.AuthHandler(ShowConfig)).Methods("GET")

	return router
}
