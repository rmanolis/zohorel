package zrcontrollers

import (
	"encoding/json"
	"net/http"
	"zohorel/zrglobals"
	"zohorel/zrmodels"
)

func EditConfig(w http.ResponseWriter, r *http.Request) {
	db := zrglobals.GetDB(r)
	config := new(zrmodels.Config)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&config)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = zrmodels.EditConfig(db, config.AuthToken,
		config.ZohoToken)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusAccepted)

}

func ShowConfig(w http.ResponseWriter, r *http.Request) {
	db := zrglobals.GetDB(r)
	config, err := zrmodels.GetConfig(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	out, err := json.Marshal(config)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(out)
}
