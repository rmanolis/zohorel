package zrcontrollers

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"zohorel/zrglobals"
	"zohorel/zrmodels"
)

func AddUser(w http.ResponseWriter, r *http.Request) {
	db := zrglobals.GetDB(r)
	user := new(zrmodels.User)
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	err = user.Insert(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
}

func EditUser(w http.ResponseWriter, r *http.Request) {
	db := zrglobals.GetDB(r)
	input, err := ioutil.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	pass := string(input)
	log.Println(pass)
	user, err := zrmodels.LoadUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}

	user.Password = pass
	err = user.Update(db)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)

}

func ListUsers(w http.ResponseWriter, r *http.Request) {
	db := zrglobals.GetDB(r)
	users, _ := zrmodels.GetUsers(db)

	for _, v := range users {
		v.Password = ""
	}
	out, err := json.Marshal(users)
	if err != nil {
		http.Error(w, "[]", http.StatusAccepted)
		return
	}
	w.Write(out)
}
