package zrcontrollers

import (
	"encoding/json"
	"log"
	"net/http"
	"zohorel/zrglobals"
	"zohorel/zrmodels"

	"github.com/goincremental/negroni-sessions"
)

type Credentials struct {
	Username string
	Password string
}

func Login(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	credentials := new(Credentials)
	err := decoder.Decode(&credentials)
	if err != nil {
		panic(err)
	}

	db := zrglobals.GetDB(r)
	user, err := zrmodels.Authenticate(db, credentials.Username,
		credentials.Password)
	if err != nil {
		http.Error(w, "Wrong password or username", http.StatusNotAcceptable)
		return
	}

	session := sessions.GetSession(r)
	session.Set("user_id", user.Id.Hex())
	w.WriteHeader(http.StatusAccepted)
}

func Logout(w http.ResponseWriter, r *http.Request) {
	session := sessions.GetSession(r)
	user_id := session.Get("user_id")
	log.Println(user_id)
	if user_id == nil {
		http.Redirect(w, r, "/", http.StatusNotAcceptable)
		return
	}
	session.Delete("user_id")
	http.Redirect(w, r, "/", http.StatusMovedPermanently)

}

func IsUser(w http.ResponseWriter, r *http.Request) {
	user, err := zrmodels.LoadUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotAcceptable)
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(user.Id.Hex()))

}
