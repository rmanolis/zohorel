package zrcontrollers

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"strconv"
	"zohorel/zrglobals"
	"zohorel/zrreloader"
	"zohorel/zrutils"
)

func LoadAccounts(w http.ResponseWriter, r *http.Request) {
	db := zrutils.GetDB(r, zrglobals.GetDBName())
	vrs := mux.Vars(r)
	sto := vrs["to"]
	sfrom := vrs["from"]
	to, err := strconv.Atoi(sto)
	if err != nil {
		http.Error(w, "not correct toIndex", http.StatusNotAcceptable)
		return
	}
	from, err := strconv.Atoi(sfrom)
	if err != nil {
		http.Error(w, "not correct fromIndex", http.StatusNotAcceptable)
		return
	}

	rows, err := zrreloader.RequestAccounts(db, to, from)
	if err != nil {
		log.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	zrreloader.SaveAccounts(db, rows)
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte("accounts have been saved"))
}
