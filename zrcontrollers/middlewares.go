package zrcontrollers

import (
	"log"
	"net/http"
	"zohorel/zrglobals"
	"zohorel/zrmodels"
	"zohorel/zrutils"
)

func AuthTokenHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		db := zrutils.GetDB(r, zrglobals.GetDBName())
		con, err := zrmodels.GetConfig(db)
		if err != nil {
			log.Println(err.Error())
			http.Error(w, "", http.StatusInternalServerError)
			return
		}

		at, ok := r.URL.Query()["token"]
		if !ok {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}
		if con.AuthToken != at[0] {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		next(w, r)

	}
}
