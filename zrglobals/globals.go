package zrglobals

import (
	"net/http"
	"zohorel/zrutils"

	"gopkg.in/mgo.v2"
)

var gdt = new(zrutils.DataStore)

func SetDT(dt *zrutils.DataStore) {
	gdt = dt
}

func GetDT() *zrutils.DataStore {
	return gdt
}

func GetDBName() string {
	return gdt.DBName
}

func GetDB(r *http.Request) *mgo.Database {
	return zrutils.GetDB(r, GetDBName())
}
