package zrutils

import (
	"errors"
	"github.com/goincremental/negroni-sessions"
	"log"
	"net/http"
)

func UserId(r *http.Request) (string, error) {
	session := sessions.GetSession(r)
	user_id := session.Get("user_id")

	if user_id == nil {
		return "", errors.New("No user")
	} else {
		return user_id.(string), nil
	}
}

func AuthHandler(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				log.Println("Recovered in f", r)
				session := sessions.GetSession(r)
				session.Delete("user_id")
				http.Redirect(w, r, "/", http.StatusUnauthorized)
				return
			}
		}()
		_, err := UserId(r)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		} else {
			next(w, r)

		}
	}
}
