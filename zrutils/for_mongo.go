package zrutils

import (
	"fmt"
	"github.com/gorilla/context"
	"gopkg.in/mgo.v2"
	"net/http"
)

func GetDB(r *http.Request, name string) *mgo.Database {
	db := context.Get(r, name)
	return db.(*mgo.Database)
}

func SetDB(r *http.Request, db *mgo.Database, name string) {
	context.Set(r, name, db)

}

type DataStore struct {
	DBName  string
	Session *mgo.Session
}

func NewDataStore(name, ip, port string) *DataStore {
	serv := fmt.Sprint(ip, ":", port)
	fmt.Println(serv)
	session, err := mgo.Dial(serv)
	if err != nil {
		panic(err)
	}
	dt := new(DataStore)
	dt.Session = session
	dt.DBName = name

	return dt
}

func (dt *DataStore) MgoMiddleware(rw http.ResponseWriter, r *http.Request, next http.HandlerFunc) {
	reqSession := dt.Session.Clone()
	defer reqSession.Close()
	db := reqSession.DB(dt.DBName)
	SetDB(r, db, dt.DBName)
	next(rw, r)
}
