package main

import (
	"log"
	"net/http"
	"time"
	"zohorel/zrcontrollers"
	"zohorel/zrglobals"
	"zohorel/zrmodels"
	"zohorel/zrreloader"
	"zohorel/zrutils"

	"github.com/codegangsta/negroni"
	"github.com/goincremental/negroni-sessions"
	"github.com/goincremental/negroni-sessions/cookiestore"
)

func loadSystem(dt *zrutils.DataStore) {
	ses := dt.Session.Clone()
	defer ses.Close()
	db := ses.DB(dt.DBName)
	zrmodels.NewUser(db, "admin", "manolis")
	zrmodels.NewConfig(db, "Q9TQA10GHDN0I4J2G2K41YBM6LWG2DLXFYI0",
		"16adc92f894cd4c108ff1236f6548be2")

}

func loadAccounts(dt *zrutils.DataStore) {
	ses := dt.Session.Clone()
	defer ses.Close()
	db := ses.DB(dt.DBName)
	log.Println("load accounts")
	rows, err := zrreloader.RequestAccounts(db, 1, 400)
	if err != nil {
		log.Println("could not reload ", err.Error())
		return
	}
	zrreloader.SaveAccounts(db, rows)
	log.Println("accounts have been updated")
}

func reloadAccounts(dt *zrutils.DataStore) {
	log.Println("Every 15 minutes will reload accounts")
	for _ = range time.Tick(15 * time.Minute) {
		loadAccounts(dt)
	}
}

func main() {
	router := zrcontrollers.LoadRoutes()
	n := negroni.Classic()
	static := negroni.NewStatic(http.Dir("static"))
	static.Prefix = "/static"
	n.Use(static)

	store := cookiestore.New([]byte("kasdhas234342342jdka11123132sdhkasdgkahsd8u33s9"))
	n.Use(sessions.Sessions("global_session_store", store))

	zrglobals.SetDT(zrutils.NewDataStore("zohorel", "127.0.0.1", "27017"))
	dt := zrglobals.GetDT()
	defer dt.Session.Close()
	n.Use(negroni.HandlerFunc(dt.MgoMiddleware))
	n.UseHandler(router)
	loadSystem(dt)
	go reloadAccounts(dt)
	n.Run(":5000")

}
