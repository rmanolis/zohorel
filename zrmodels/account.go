package zrmodels

import (
	"zohorel/zrmodels/zrtablenames"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Account struct {
	Id         bson.ObjectId `bson:"_id,omitempty"`
	No         int           `json:"no"`
	AccountId  string        `json:"account_id"`
	AccountNum string        `json:"account_number"`
	IsActive   bool          `json:"is_active"`
	IsPayment  bool          `json:"is_payment"`
}

func (acc *Account) ByNo(db *mgo.Database, no int) error {
	c := db.C(zrtablenames.Account)
	return c.Find(bson.M{"no": no}).One(&acc)
}

func (acc *Account) ByAccountId(db *mgo.Database, id string) error {
	c := db.C(zrtablenames.Account)
	return c.Find(bson.M{"accountid": id}).One(&acc)
}

func (acc *Account) ByAccountNum(db *mgo.Database, id string) error {
	c := db.C(zrtablenames.Account)
	return c.Find(bson.M{"accountnum": id}).One(&acc)
}

func (acc *Account) Insert(db *mgo.Database) error {
	c := db.C(zrtablenames.Account)
	acc.Id = bson.NewObjectId()
	return c.Insert(&acc)
}

func (acc *Account) Update(db *mgo.Database) error {
	c := db.C(zrtablenames.Account)
	return c.UpdateId(acc.Id, acc)
}

func (acc *Account) Save(db *mgo.Database) error {
	if acc.Id.Valid() {
		return acc.Update(db)
	} else {
		return acc.Insert(db)
	}
}
