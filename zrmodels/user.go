package zrmodels

import (
	"errors"
	"log"
	"net/http"
	"zohorel/zrglobals"
	"zohorel/zrmodels/zrtablenames"
	"zohorel/zrutils"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type User struct {
	Id       bson.ObjectId `bson:"_id,omitempty"`
	Username string
	Password string
}

func LoadUser(r *http.Request) (*User, error) {
	user := new(User)
	db := zrglobals.GetDB(r)
	if db == nil {
		return nil, errors.New("user: database is not connected")
	}
	id, err := zrutils.UserId(r)
	if err != nil {
		return nil, err
	}
	if !bson.IsObjectIdHex(id) {
		return nil, errors.New("user: it is not an ID")
	}
	log.Println(id)
	err = db.C(zrtablenames.User).FindId(bson.ObjectIdHex(id)).One(&user)

	return user, err
}

func Authenticate(db *mgo.Database, username, password string) (*User, error) {
	c := db.C(zrtablenames.User)
	user := new(User)
	err := c.Find(bson.M{"username": username, "password": password}).One(&user)
	return user, err
}

func NewUser(db *mgo.Database, name, password string) (*User, error) {
	user := new(User)
	user.Id = bson.NewObjectId()
	user.Password = password
	user.Username = name
	c := db.C(zrtablenames.User)
	n, err := c.Find(bson.M{"username": name}).Count()
	if n > 0 {
		return nil, errors.New("user exists already")
	}
	err = c.Insert(&user)
	return user, err
}

func (user *User) Insert(db *mgo.Database) error {
	c := db.C(zrtablenames.User)
	user.Id = bson.NewObjectId()
	return c.Insert(&user)
}

func (user *User) Update(db *mgo.Database) error {
	c := db.C(zrtablenames.User)
	return c.UpdateId(user.Id, &user)
}

func GetUsers(db *mgo.Database) ([]User, error) {
	c := db.C(zrtablenames.User)
	var users = []User{}
	err := c.Find(bson.M{}).All(&users)
	return users, err
}
