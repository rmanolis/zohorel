package zrmodels

import (
	"zohorel/zrmodels/zrtablenames"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

type Config struct {
	Id        bson.ObjectId `bson:"_id,omitempty"`
	AuthToken string
	ZohoToken string
}

func NewConfig(db *mgo.Database, at, zt string) (*Config, error) {
	c := db.C(zrtablenames.Config)
	n, err := c.Find(bson.M{}).Count()
	if n > 0 {
		return nil, err
	}
	config := new(Config)
	config.Id = bson.NewObjectId()
	config.AuthToken = at
	config.ZohoToken = zt
	err = c.Insert(&config)
	return config, err
}

func GetConfig(db *mgo.Database) (*Config, error) {
	c := db.C(zrtablenames.Config)
	config := new(Config)
	err := c.Find(bson.M{}).One(&config)
	return config, err
}

func EditConfig(db *mgo.Database, at, zt string) error {
	c := db.C(zrtablenames.Config)
	config, err := GetConfig(db)
	if err != nil {
		return err
	}
	config.AuthToken = at
	config.ZohoToken = zt
	return c.UpdateId(config.Id, config)
}
