package zrreloader

import (
	"encoding/json"
	"gopkg.in/mgo.v2"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"zohorel/zrmodels"
)

type Content struct {
	Val     string `json:"val"`
	Content string `json:"content"`
}

type Row struct {
	No string    `json:"no"`
	FL []Content `json:"FL"`
}
type Accounts struct {
	Row []Row `json:"row"`
}
type Result struct {
	Accounts Accounts `json:"Accounts"`
}
type Response struct {
	Result Result `json:"result"`
}

type Records struct {
	Response Response `json:"response"`
}

func RequestAccounts(db *mgo.Database, toIndex, fromIndex int) ([]Row, error) {
	config, err := zrmodels.GetConfig(db)
	if err != nil {
		log.Println(err)
		return nil, err
	}
	resp, err := http.Get("https://crm.zoho.com/crm/private/json/Accounts/getRecords?authtoken=" +
		config.ZohoToken + "&scope=crmapi&fromIndex=" + strconv.Itoa(toIndex) + "&toIndex=" + strconv.Itoa(fromIndex))
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	contents, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}

	recs := new(Records)
	err = json.Unmarshal(contents, &recs)
	if err != nil {
		log.Println(err.Error())
		return nil, err
	}
	return recs.Response.Result.Accounts.Row, nil
}

func SaveAccounts(db *mgo.Database, rows []Row) {
	for _, row := range rows {
		rn, err := strconv.Atoi(row.No)
		acc := new(zrmodels.Account)
		err = acc.ByNo(db, rn)
		if err != nil {
			acc.No = rn
		}
		for _, v := range row.FL {
			if v.Val == "ACCOUNTID" {
				err = acc.ByAccountId(db, v.Content)
				if err == nil {
					acc.No = rn
				}
				acc.AccountId = v.Content
			}
			if v.Val == "Active" {
				acc.IsActive, err = strconv.ParseBool(v.Content)
				if err != nil {
					log.Println(err.Error())
				}
			}
			if v.Val == "Payment" {
				acc.IsPayment, err = strconv.ParseBool(v.Content)
				if err != nil {
					log.Println(err.Error())
				}
			}
			if v.Val == "Account Number" {
				acc.AccountNum = v.Content
			}
		}
		err = acc.Save(db)
		if err != nil {
			log.Println(err.Error())
		}
	}

}
