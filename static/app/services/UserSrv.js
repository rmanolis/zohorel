app.factory("UserSrv",function($http){
  var obj = {};
 
  obj.getUsers = function(){
    return $http.get("/users");
  }

  obj.editUser = function(pass){
    return $http.put("/users",pass);
  }

  obj.addUser = function(user){
    return $http.post("/users",user);
  }

  return obj;
})
