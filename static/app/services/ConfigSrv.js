app.factory("ConfigSrv",function($http){
  var obj = {};
  
  obj.editConfig = function(config){
    return $http.put("/config",config);
  }

  obj.getConfig = function(){
    return $http.get("/config");
  }

  return obj;
})
