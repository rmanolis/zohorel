app.controller('LoginCtrl', function($scope, $location, $rootScope, AuthSrv){
  $scope.user = {}
  $scope.user.Username = "";
  $scope.user.Password = "";
  $scope.login = function (user) {
    AuthSrv.login(user).
      success(function (data) {
        console.log(data);
        $rootScope.$broadcast("successful:login");
        $location.path("/");
     })
    .error(function(error){
      console.log(error)
        alert('Wrong username and password');
    });
  };

 
});
