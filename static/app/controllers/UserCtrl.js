app.controller("UserCtrl",function($scope, UserSrv,toastr){
  $scope.password = "";
  $scope.savePassword = function(password){
    UserSrv.editUser(password).success(function(){
      toastr.info("Password saved");
      $scope.password ="";
      $scope.rep_password="";
    }).error(function(data){
      toastr.error("Error: " + data);
    })
  }

  $scope.users = [];
  function getUsers(){
    UserSrv.getUsers().success(function(users){
      console.log(users);
      $scope.users = users;
    })
  }
  getUsers();

  $scope.new_user = {};
  $scope.addUser = function(user){
    UserSrv.addUser(user).success(function(){
      toastr.info("User saved");
      getUsers();
    }).error(function(data){
      toastr.error("Error: " + data);
    })
  }

  


})

