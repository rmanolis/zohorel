app.controller("ConfigCtrl",function($scope, ConfigSrv, toastr){
  $scope.config={};

  ConfigSrv.getConfig().success(function(config){
    $scope.config = config;
  })

  $scope.save = function(config){
    ConfigSrv.editConfig(config).success(function(){
      toastr.info("Config saved")
    }).error(function(data){
      toastr.error("Error: "+data)
    })
  }
})


